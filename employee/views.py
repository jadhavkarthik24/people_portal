from .serializers import *
from .models import Employee
from django.db.models import Q
from django.http import Http404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated


class LoginView(APIView):
    """ 
    @API -> login API,
    @Method -> POST,  
    @Parameters -> email,password
    """
    serializer_class = EmployeeLoginSerializer
    permission_classes = (AllowAny, )

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        valid = serializer.is_valid(raise_exception=True)

        if valid:
            status_code = status.HTTP_200_OK
            employee = serializer.validate(request.data)

            response = {
                'success': True,
                'message': 'User logged in successfully',
                'token': employee['token'],
                'email': employee['email'],
                'role': employee['role']
            }

            return Response(response, status=status_code)


class CreateEmployeeView(APIView):
    """ 
    @API -> Add New Employee API,
    @Method -> POST,  
    @Parameters -> email,password,employee_id,role,name
    """
    serializer_class = CreateEmployeeSerializer
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        user = request.user
        if user.role != 1 and user.role != 2:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            serializer = self.serializer_class(data=request.data)
            valid = serializer.is_valid(raise_exception=True)

            if valid:
                obj = serializer.save()
                last_id = obj.pk
                emp_id = "BP{}".format(format(last_id, '03d'))
                obj.employee_id = emp_id
                obj.save()
                status_code = status.HTTP_201_CREATED

                response = {
                    'success': True,
                    'message': 'User successfully created',
                    'user': {
                        "id" : last_id,
                        "email": serializer.data["email"],
                        "employee_id": emp_id,
                        "role": serializer.data["role"],
                        "position": serializer.data["position"],
                        "team": serializer.data["team"],
                        "phone_no": serializer.data["phone_no"]
                    }
                }

                return Response(response, status=status_code)


class EmployeesListView(APIView):
    """ 
    @API -> Read Employees API,
    @Method -> POST, 
    @Description -> Returns list of employees 
    """
    serializer_class = EmployeesListSerializer
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        user = request.user
        if user.role == 3:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            employees = Employee.objects.filter(is_deleted=0)
            if 'search' in request.GET:
                search = request.GET['search']
                employees = employees.filter(
                    Q(name__icontains=search) | Q(team__icontains=search))
            serializer = self.serializer_class(employees, many=True)
            response = {
                'success': True,
                'message': 'Data fetched successfully',
                'employees': serializer.data
            }
            return Response(response, status=status.HTTP_200_OK)


class LogoutView(APIView):
    """ 
    @API -> Read Employees API,
    @Method -> POST,  
    @Description -> Deletes the token to force a login
    """
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        request.user.auth_token.delete()
        response = {
            'success': True,
            'message': 'User logged out successfully',
        }
        return Response(response, status=status.HTTP_200_OK)


class EmployeeRUDView(APIView):
    """ 
    @API -> Edit.Delete,Get Single Employees API,
    @Method -> GET,PUT,DELETE
    @Parameter -> id  
    """
    permission_classes = (IsAuthenticated, )
    serializer_class = EmployeeRUDSerializer

    def get_object(self, pk):
        try:
            return Employee.objects.get(pk=pk)
        except Employee.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        user = self.get_object(pk)
        serializer = self.serializer_class(user)
        response = {
            'success': True,
            'message': 'Fetched a employee details successfully',
            'user': serializer.data
        }
        return Response(response, status=status.HTTP_200_OK)

    def put(self, request, pk):
        user = request.user
        if user.role != 1 and user.role != 2:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            employee = self.get_object(pk)
            serializer = self.serializer_class(employee, data=request.data)
            if serializer.is_valid():
                serializer.save()
                response = {
                    'success': False,
                    'message': 'Data updated successfully',
                    'user': serializer.data
                }
                return Response(response, status=status.HTTP_200_OK)
            response = {
                'success': False,
                'message': serializer.errors
            }
            return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    def delete(self, request, pk):
        user = request.user
        if user.role != 1:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            user = self.get_object(pk)
            user.is_deleted = 1
            user.save()
            """ deleting user token """
            Token.objects.filter(user=user).delete()
            response = {
                'success': True,
                'message': 'Deleted the entry successfully'
            }
            return Response(response, status=status.HTTP_200_OK)
