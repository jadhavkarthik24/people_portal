from .models import Employee, timezone
from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token


class EmployeeLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(max_length=128, write_only=True)

    def validate(self, data):
        email = data['email']
        password = data['password']
        employee = authenticate(email=email, password=password)

        if employee is None:
            raise serializers.ValidationError("Invalid login credentials")

        try:
            token_exists = Token.objects.filter(user=employee).exists()
            if token_exists:
                token = employee.auth_token.key
            else:
                token = Token.objects.create(user=employee)
                token = token.key

            validation = {
                'email': employee.email,
                'role': employee.role,
                'token': token,
            }
            return validation
        except Employee.DoesNotExist:
            raise serializers.ValidationError("Invalid login credentials")


class CreateEmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = (
            'email',
            'name',
            'password',
            'role',
            'position',
            'team',
            'phone_no'
        )

    def create(self, validated_data):
        employee = Employee.objects.create_user(**validated_data)
        return employee


class EmployeesListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = ('id', 'email', 'name', 'employee_id', 'role',
                  'created_date', 'position', 'team', 'phone_no')


class EmployeeRUDSerializer(serializers.Serializer):
    """ Read,Update,Delete Serializer """
    name = serializers.CharField(max_length=128)
    email = serializers.CharField(max_length=128)
    role = serializers.CharField(max_length=100)
    position = serializers.CharField(max_length=64)
    team = serializers.CharField(max_length=64)
    phone_no = serializers.CharField(max_length=16)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Users` instance, given the validated data.
        """
        instance.name = validated_data.get(
            'name', instance.name)
        instance.email = validated_data.get(
            'email', instance.email)
        instance.role = validated_data.get(
            'role', instance.role)
        instance.position = validated_data.get(
            'position', instance.position)
        instance.team = validated_data.get(
            'team', instance.team)
        instance.phone_no = validated_data.get(
            'phone_no', instance.phone_no)
        instance.save()
        return instance
