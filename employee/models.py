from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
# from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    """
    Custom user model where the email address is the unique identifier
    and has an is_admin field to allow access to the admin app 
    """

    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError(_("The email must be set"))
        if not password:
            raise ValueError(_("The password must be set"))
        email = self.normalize_email(email)

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('role', 1)

        if extra_fields.get('role') != 1:
            raise ValueError('Superuser must have role of Global Admin')
        return self.create_user(email, password, **extra_fields)


class Employee(AbstractBaseUser):
    """ table which employee details are added """
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=128)
    employee_id = models.CharField(max_length=16, blank=True)
    position = models.CharField(max_length=64, blank=True)
    team = models.CharField(max_length=64, blank=True)
    phone_no = models.CharField(max_length=32, blank=True)
    is_deleted = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(_('date joined'), default=timezone.now)
    modified_date = models.DateTimeField(default=timezone.now)

    CHOICES = (
        (1, 'Admin'),
        (2, 'Engg Manager'),
        (3, 'Office Manager'),
        (4, 'Employee')
    )
    role = models.PositiveSmallIntegerField(
        choices=CHOICES, blank=True, null=True, default=4)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        verbose_name = 'employee'
        verbose_name_plural = 'employees'

    def __str__(self):
        return self.email

    # @property
    # def is_staff(self):
    #     return self.role

    # def has_module_perms(self, app_label):
    #     "Does the user have permissions to view the app `app_label`?"
    #     # Simplest possible answer: Yes, always
    #     return True

    # def has_perm(self, perm, obj=None):
    #     "Does the user have a specific permission?"
    #     # Simplest possible answer: Yes, always
    #     return True
