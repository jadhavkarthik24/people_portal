from django.urls import path
from .views import *

urlpatterns = [
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('add/', CreateEmployeeView.as_view(), name="add_employee"),
    path('list/', EmployeesListView.as_view(), name="employee_read"),
    path('<int:pk>/', EmployeeRUDView.as_view(),name="employee-rud"),
]
