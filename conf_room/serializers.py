from .models import ConferenceRoom
from rest_framework import serializers


class CreateConfRoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConferenceRoom
        fields = (
            'name',
            'booking_email',
            'sitting',
        )

    def create(self, validated_data):
        return ConferenceRoom.objects.create(**validated_data)


class RoomsListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConferenceRoom
        fields = ('id', 'room_id', 'name', 'booking_email', 'sitting',
                  'current_status', 'created_date')


class ConfRoomRUDSerializer(serializers.Serializer):
    """ Read,Update,Delete Serializer """
    current_status = serializers.CharField(max_length=128)
   
    def update(self, instance, validated_data):
        """
        Update and return an existing `Users` instance, given the validated data.
        """
        instance.current_status = validated_data.get(
            'current_status', instance.current_status)
        instance.save()
        return instance
