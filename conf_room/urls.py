from django.urls import path
from .views import *

urlpatterns = [
    path('create/', CreateConfRoomView.as_view(), name="create_new_room"),
    path('list/', RoomsListView.as_view(), name="conference_rooms_list"),
    path('<int:pk>/', ConfRoomRUDView.as_view(), name="conf_room_rud"),
]
