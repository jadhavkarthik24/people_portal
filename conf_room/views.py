from .serializers import *
from django.db.models import Q
from django.http import Http404
from rest_framework import status
from .models import ConferenceRoom
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated


class CreateConfRoomView(APIView):
    """ 
    @API -> Create New Room API,
    @Method -> POST,  
    @Parameters -> name,email,sitting
    """
    serializer_class = CreateConfRoomSerializer
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        user = request.user
        if user.role != 1 and user.role != 3:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            serializer = self.serializer_class(data=request.data)
            valid = serializer.is_valid(raise_exception=True)

            if valid:
                serializer.save()
                last_entry = ConferenceRoom.objects.latest('id')
                last_id = last_entry.pk
                last_entry.room_id = "CFR{}".format(format(last_id, '02d'))
                last_entry.room_added_by = user.email
                last_entry.save()
                serializer.validated_data['room_id'] = last_entry.room_id
                status_code = status.HTTP_201_CREATED

                response = {
                    'success': True,
                    'message': 'Room successfully created',
                    'conf_room': serializer.validated_data
                }

                return Response(response, status=status_code)


class RoomsListView(APIView):
    """ 
    @API -> Read Employees API,
    @Method -> POST, 
    @Description -> Returns list of rooms 
    """
    serializer_class = RoomsListSerializer
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        user = request.user
        if user.role == 2:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            rooms = ConferenceRoom.objects.filter(is_deleted=0)
            if 'search' in request.GET:
                search = request.GET['search']
                employees = rooms.filter(
                    Q(name__icontains=search) | Q(sitting__icontains=search))
            serializer = self.serializer_class(employees, many=True)
            response = {
                'success': True,
                'message': 'Data fetched successfully',
                'employees': serializer.data
            }
            return Response(response, status=status.HTTP_200_OK)


class ConfRoomRUDView(APIView):
    """ 
    @API -> Edit.Delete,Get Single Conference Room API,
    @Method -> GET,PUT,DELETE
    @Parameter -> id  
    """
    permission_classes = (IsAuthenticated, )
    serializer_class = ConfRoomRUDSerializer

    def get_object(self, pk):
        try:
            return ConferenceRoom.objects.get(pk=pk)
        except ConferenceRoom.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        user = self.get_object(pk)
        serializer = self.serializer_class(user)

        response = {
            'success': True,
            'message': 'Fetched a conference room details successfully',
            'user': {
                "id": user.id,
                "room_id": user.room_id,
                "name": user.name,
                "booking_email": user.booking_email,
                "current_status": user.current_status,
                "created_date": user.created_date
            }
        }
        return Response(response, status=status.HTTP_200_OK)

    def put(self, request, pk):
        user = request.user
        if user.role != 1 and user.role != 3:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            employee = self.get_object(pk)
            serializer = self.serializer_class(employee, data=request.data)
            if serializer.is_valid():
                serializer.save()
                response = {
                    'success': False,
                    'message': 'Data updated successfully',
                    'user': serializer.data
                }
                return Response(response, status=status.HTTP_200_OK)
            response = {
                'success': False,
                'message': serializer.errors
            }
            return Response(response, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    def delete(self, request, pk):
        user = request.user
        if user.role != 1:
            response = {
                'success': False,
                'message': 'You are not authorized to perform this action'
            }
            return Response(response, status.HTTP_403_FORBIDDEN)
        else:
            user = self.get_object(pk)
            user.is_deleted = 1
            user.save()

            response = {
                'success': True,
                'message': 'Deleted the entry successfully'
            }
            return Response(response, status=status.HTTP_200_OK)
