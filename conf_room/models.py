from django.db import models
from django.utils import timezone


class ConferenceRoom(models.Model):
    room_id = models.CharField(max_length=6, unique=True, editable=False)
    name = models.CharField(max_length=64)
    booking_email = models.EmailField(unique=True)
    sitting = models.PositiveSmallIntegerField()
    STATUS = (
        ('Available', 'Available'),
        ('Booked', 'Booked')
    )
    current_status = models.CharField(
        choices=STATUS, max_length=12, default="Available")
    created_date = models.DateTimeField(default=timezone.now)
    room_added_by = models.EmailField(null=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'room'
        get_latest_by = ['id']

    def __str__(self):
        return self.room_id
